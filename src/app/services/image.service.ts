import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private callback: ((error: any, result?: any) => void) | undefined;

  constructor(
  ) {
  }

  public setCallback(callback: (error: any, result?: string) => void) {
    this.callback = callback;
  }

  public processImage(data: any): void {
    const img = new Image();
    img.onload = () => {
      this.done({ dataUri: data.dataUri });
    };
    img.onerror = () => {
      this.done({
        errorMessage: `Not an image file`
      });
    };
    img.src = data.dataUri;
  }

  public convertDataUriToBlob(dataUri: string): Blob {
    const pattern = /^data:([^\/]+\/[^;]+)?(;charset=([^;]+))?(;base64)?,/i;
    const matches = dataUri.match(pattern);
    if (matches == null) {
      throw new Error('data: uri did not match scheme');
    } else {
      const prefix = matches[0];
      //let contentType = matches[1];
      // let charset = matches[3]; -- not used.
      const isBase64 = matches[4] != null;
      // remove the prefix
      const encodedBytes = dataUri.slice(prefix.length);
      // decode the bytes
      const decodedBytes = isBase64 ? atob(encodedBytes) : encodedBytes;

      // Write the bytes of the string to a typed array
      const charCodes = Object.keys(decodedBytes)
        .map<number>(Number)
        .map<number>(decodedBytes.charCodeAt.bind(decodedBytes));

      // Build blob with typed array
      return new Blob([new Uint8Array(charCodes)], { type: 'image/jpeg' });
    }
  }

  private done({ errorMessage, dataUri }: { errorMessage?: string; dataUri?: string }) {
    if (this.callback) {
      if (errorMessage) {
        this.callback(new Error(errorMessage));
      } else {
        this.callback(null, dataUri);
      }
    }
  }
}
