import { Component, OnInit, ViewChild } from '@angular/core';
import { LoadingService } from '../../services/loading.service';
import { ImageService } from '../../services/image.service';
import { API } from 'aws-amplify';
import { AlertController } from '@ionic/angular';

const ADD_IMAGE_PLACEHOLDER_URI = '/assets/images/add_image_camera_photo.png';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})
export class HomePage implements OnInit {
  @ViewChild('hiddenFileInputProbe') hiddenFileInputProbe: any;
  @ViewChild('hiddenFileInputCandidate') hiddenFileInputCandidate: any;
  placeholderImageUri = ADD_IMAGE_PLACEHOLDER_URI;
  probeImageUri: string;
  candidateImageUri: string;
  private processingImageId: string;

  constructor(
    private loadingService: LoadingService,
    private imageService: ImageService,
    private alertCtrl: AlertController
  ) {
  }

  ngOnInit() {
    this.imageService.setCallback(this.imageCallback.bind(this));
  }

  fileInputChangeListener({ id, event }: { id: string; event: any }) {
    console.log(`fileInputChangeListener()`);
    // this.loadingService.setIsLoading(true);
    this.processingImageId = id;
    const file: File = event.target.files[0];
    const fileReader: FileReader = new FileReader();
    fileReader.onload = evt => {
      const dataUri = (evt as any).target.result;  //casting evt to any to avoid typescript warning message
      console.log(`dataUri`, dataUri);
      // if (this.processingImageId === `Probe`) {
      //   this.probeImageUri = dataUri;
      // } else {
      //   this.candidateImageUri = dataUri;
      // }
      this.imageService.processImage({
        file,
        dataUri
      });
    };
    fileReader.readAsDataURL(file);
  }

  async selectImage(id) {
    this[`hiddenFileInput${id}`].nativeElement.value = ``;
    this[`hiddenFileInput${id}`].nativeElement.click();
  }

  async match() {
    this.loadingService.setIsLoading(true);
    try {
      const body = {
        probe: this.probeImageUri,
        candidate: this.candidateImageUri
      };
      const result = await API.post(
        `SourceAfisApi`,
        `/verify`, {
          body
        });
      this.loadingService.setIsLoading(false);
      console.log(`Verify result`, result);
      const alert = await this.alertCtrl.create({
        header: 'Verify Result',
        message: `Your match score was ${result.score}`,
        buttons: [{text: `DONE`}]
      });
      await alert.present();
    } catch (error) {
      this.loadingService.setIsLoading(false);
      const alert = await this.alertCtrl.create({
        header: 'Verify Error',
        subHeader: 'Unable to match fingerprints',
        message: error.message,
        buttons: [{text: `OK`}]
      });
      await alert.present();
    }
  }

  private imageCallback(error: any, result?: string): void {
    this.loadingService.setIsLoading(false);
    if (error) {
      console.error('ImageService Error: ', error);
    } else {
      console.log(`dataUri`, result);
      if (this.processingImageId === `Probe`) {
        this.probeImageUri = result;
      } else {
        this.candidateImageUri = result;
      }
    }
  }
}
